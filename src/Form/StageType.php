<?php

namespace App\Form;

use App\Form\FormationType;
use App\Entity\Stage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use App\Entity\Formation;

class StageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titreStage')
            ->add('descriptionStage')
            ->add('emailContact')
            ->add('nomContact')
            ->add('dateDebutStage')
            ->add('dateFinStage')
            ->add('Formation', FormationType::class, ['data_class'=> null])
            //->add('entreprises')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stage::class,
        ]);
    }
}
