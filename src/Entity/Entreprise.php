<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as assert; 



/**
 * @ORM\Entity(repositoryClass="App\Repository\EntrepriseRepository")
 */
class Entreprise
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @assert\NotBlank 
     * @assert\Length( 
     *          min = 4, 
     *          minMessage = "Le nom doit faire au minimum {{ limit }} caractères" 
     * ) 
     */
    private $nomEntreprise;

    /**
     * @ORM\Column(type="text")
     * @assert\NotBlank 
     */
    private $descriptionEntreprise;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Regex(pattern="/ rue|avenue|boulevard|impasse|allée|place|route|voie /", message = "Le type de route/voie semble incorrect") 
     * @Assert\Regex(pattern="/ [0-9]{5} /", message = "Il semble y avoir un problème avec le code postal") 
     * @Assert\Regex(pattern="/^[1-9][0-9]{0,2}(bis| bis)? /", message = "Le numéro de rue semble incorrect") 
     */
    private $adresseEntreprise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @assert\NotBlank 
     * @assert\Url 
     */
    private $siteEntreprise;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $numeroTelephoneEntreprise;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stage", mappedBy="entreprises")
     */
    private $stages;

    public function __construct()
    {
        $this->stages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomEntreprise(): ?string
    {
        return $this->nomEntreprise;
    }

    public function setNomEntreprise(string $nomEntreprise): self
    {
        $this->nomEntreprise = $nomEntreprise;

        return $this;
    }

    public function getDescriptionEntreprise(): ?string
    {
        return $this->descriptionEntreprise;
    }

    public function setDescriptionEntreprise(string $descriptionEntreprise): self
    {
        $this->descriptionEntreprise = $descriptionEntreprise;

        return $this;
    }

    public function getAdresseEntreprise(): ?string
    {
        return $this->adresseEntreprise;
    }

    public function setAdresseEntreprise(string $adresseEntreprise): self
    {
        $this->adresseEntreprise = $adresseEntreprise;

        return $this;
    }

    public function getSiteEntreprise(): ?string
    {
        return $this->siteEntreprise;
    }

    public function setSiteEntreprise(?string $siteEntreprise): self
    {
        $this->siteEntreprise = $siteEntreprise;

        return $this;
    }

    public function getNumeroTelephoneEntreprise(): ?string
    {
        return $this->numeroTelephoneEntreprise;
    }

    public function setNumeroTelephoneEntreprise(string $numeroTelephoneEntreprise): self
    {
        $this->numeroTelephoneEntreprise = $numeroTelephoneEntreprise;

        return $this;
    }

    /**
     * @return Collection|Stage[]
     */
    public function getStages(): Collection
    {
        return $this->stages;
    }

    public function addStage(Stage $stage): self
    {
        if (!$this->stages->contains($stage)) {
            $this->stages[] = $stage;
            $stage->setEntreprises($this);
        }

        return $this;
    }

    public function removeStage(Stage $stage): self
    {
        if ($this->stages->contains($stage)) {
            $this->stages->removeElement($stage);
            // set the owning side to null (unless already changed)
            if ($stage->getEntreprises() === $this) {
                $stage->setEntreprises(null);
            }
        }

        return $this;
    }
}
