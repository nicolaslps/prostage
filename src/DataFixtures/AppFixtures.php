<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Entreprise;
use App\Entity\Stage;
use App\Entity\Formation;
use App\Entity\User;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        /*********************************************************************************************
         **********************************************USERS TEST************************************
        *********************************************************************************************/

        $nicolas = new User();
        $nicolas->setPrenom("Nicolas");
        $nicolas->setNom("Lopes");
        $nicolas->setEmail("nicolas@gmail.com");
        $nicolas->setRoles(['ROLE_USER','ROLE_ADMIN']);
        $nicolas->setPassword('$2y$10$dVinwgFDq.RW2u2DJebL5.0Q0b.fMsFy6q3SL5XUm3la.WGdZXYJ2');
        $manager->persist($nicolas);
        


        $michel = new User();
        $michel->setPrenom("Michel");
        $michel->setNom("Gpasdidé");
        $michel->setEmail("michel@gmail.com");
        $michel->setRoles(['ROLE_USER']);
        $michel->setPassword('$2y$10$lXMV3HdwjKjoBm9NwpOjJOoQzhsGg22nrwiTHrBQpFaPCAm/ysDO6');
        $manager->persist($michel);

        /*********************************************************************************************
         **********************************************Formations************************************
        *********************************************************************************************/
        //Création classique des différents éléments d'une Formations
        $dutInfo = new Formation();
        $dutInfo -> setNomFormation("DUT Informatique");
        $dutInfo -> setSigleFormation("DUT Info");

        $dutTic = new Formation();
        $dutTic -> setNomFormation("DUT TIC");
        $dutTic -> setSigleFormation("DUT TIC");

        $licenceProMulti = new Formation();
        $licenceProMulti -> setNomFormation("Licence professionnelle Multimedia");
        $licenceProMulti -> setSigleFormation("LP Multimedia");

        $listeFormations = array($dutInfo,$dutTic,$licenceProMulti);
        foreach ($listeFormations as $uneFormation){
            $manager->persist($uneFormation);
        }
        //$nbFormation = 3;
        /*********************************************************************************************
         **********************************************Entreprises************************************
        *********************************************************************************************/
        
        $faker = \Faker\Factory::create('fr_FR');

        $listeEntreprise=array();
        for($i=1;$i<=15;$i++){
            /*Création classique des différents éléments d'une Entreprise*/
            $entreprise = new Entreprise();
            
            $entreprise -> setNomEntreprise($faker->company());

            $entreprise -> setDescriptionEntreprise($faker->realText($maxNbChars = 200, $indexSize = 2));

            $entreprise -> setAdresseEntreprise($faker->streetAddress());

            $entreprise -> setSiteEntreprise($faker->url());
    
            $entreprise -> setNumeroTelephoneEntreprise($faker->phoneNumber());
            
            array_push($listeEntreprise,$entreprise); 
            
            $manager->persist($entreprise);
        }

        /*********************************************************************************************
         **********************************************Stages*****************************************
        *********************************************************************************************/

        for($i_stage=1;$i_stage<=15;$i_stage++){
            /*Création classique des différents éléments d'un stage*/
            $stage = new Stage();
            $stage ->setTitreStage($faker->catchPhrase());
            $stage ->setDescriptionStage($faker->realText($maxNbChars = 400, $indexSize = 2));
            $stage ->setEmailContact($faker->email());
            $stage ->setNomContact($faker->name($gender = 'male'|'female'));
            $stage ->setDateDebutStage($faker->dateTimeBetween($startDate = 'now', $endDate = '+2 years', $timezone = null, $format = 'd-m-Y'));
            $stage ->setDateFinStage($faker->dateTimeBetween($startDate = $stage->getDateDebutStage(), $endDate = '+2 years', $timezone = null, $format = 'd-m-Y'));
            /* Association de l'entreprise*/
            $entrepriseDuStage = $faker->randomElement($array = $listeEntreprise);
            $stage->setEntreprises($entrepriseDuStage);
            $entrepriseDuStage -> addStage($stage);
            /* association des formations*/ 
            /* sélection nombre de formation concernée*/
            $nbFormationConcernee=$faker->numberBetween($min = 1, $max = 3);
            for($i_formationLiee=1; $i_formationLiee<=$nbFormationConcernee; $i_formationLiee++ ){
                /* sélection de la formation concernée*/
                $numeroFormation=$faker->numberBetween($min = 1, $max = 7);
                
                switch ($numeroFormation) {
                    case 1:
                        $stage->addFormation($dutInfo);
                        break;
                    case 2:
                        $stage->addFormation($dutTic);
                        break;
                    case 3:
                        $stage->addFormation($licenceProMulti);
                        break;
                    case 4:
                        $stage->addFormation($dutInfo);
                        $stage->addFormation($licenceProMulti);
                        break;
                    case 5:
                        $stage->addFormation($dutInfo);
                        $stage->addFormation($licenceProMulti);
                        break;
                    case 6:
                        $stage->addFormation($dutInfo);
                        $stage->addFormation($dutTic);
                        $stage->addFormation($licenceProMulti);
                        break;
                    case 7:
                        $stage->addFormation($licenceProMulti);
                        $stage->addFormation($dutTic);
                        break;
                  }


                /*
                $stage -> addFormation($listeEntreprise[$numeroFormation]);
                $listeEntreprise[$numeroFormation]->addStage($stage);
               
                $manager->persist($listeEntreprise[$numeroFormation]);*/
            }

            
            $manager->persist($entrepriseDuStage);
            $manager->persist($stage);
            

        }

        $manager->flush();
    }
}
