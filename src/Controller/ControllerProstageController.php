<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;



use App\Entity\Entreprise;
use App\Entity\Formation;
use App\Entity\Stage;


use App\Form\EntrepriseType;
use App\Form\StageType;
use App\Form\FormationType;

class ControllerProstageController extends AbstractController
{
    /**
     * @Route("/prostage", name="controller_prostage")
     */
    public function index()
    {
        return $this->render('controller_prostage/index.html.twig', [
            'controller_name' => 'ControllerProstageController',
        ]);
    }


    /**
     * @Route("/", name="accueil")
     */   
    public function accueil(){
        $repositeStagListe = $this->getDoctrine()->getRepository(Stage::class); 
        $listeStage= $repositeStagListe->findAll();  
        return $this->render("controller_prostage/accueil.html.twig", ["listeStage"=>$listeStage]);
    }





    /**
     * @Route("/entreprises", name="entreprises")
     */
    public function entreprises(){
         $repositeEntreprises = $this->getDoctrine()->getRepository(Entreprise::class); 
        $entreprise= $repositeEntreprises->findAll();  
        return $this->render("controller_prostage/entreprises.html.twig", ["listeEntreprises"=>$entreprise]); 
   }

    /**
     * @Route("/entreprises/{param}", name="stageParEntreprise")
     */   
    public function stageParEntreprise($param){
        $repositeStagListe = $this->getDoctrine()->getRepository(Stage::class); 
        $listeStage= $repositeStagListe->findByEntreprise($param);  
        return $this->render("controller_prostage/accueil.html.twig", ["listeStage"=>$listeStage]);
    }


    
    
    
    /**
     * @Route("/creation_entreprises", name="creationNouvelleEntreprise")
     */
    public function ajouterEntreprises(Request $request)
    {
        $entreprise = new Entreprise();

        $formulaireEntreprise = $this->createForm(EntrepriseType::class, $entreprise);

        $formulaireEntreprise->handleRequest($request);
            
        if ($formulaireEntreprise->isSubmitted() && $formulaireEntreprise->isValid()) {
            $this->getDoctrine()->getManager()->persist($entreprise);
            $this->getDoctrine()->getManager()->flush(); 
        }
            
        return $this->render("/controller_prostage/formEntreprises.html.twig", ['vueFormulaireEntreprise'=>$formulaireEntreprise->createView(), 'action'=>"ajouter"]);
            
    }


    /**
     * @Route("/entreprise/modifier/{nomEntreprise}", name="modificationEntreprise")
     */
    public function modifierEntreprises(Request $request, Entreprise $entreprise, ObjectManager $manager )
    {
        
        $formulaireEntreprise = $this->createForm(EntrepriseType::class, $entreprise);

         $formulaireEntreprise->handleRequest($request) ;
            
        if ($formulaireEntreprise->isSubmitted() && $formulaireEntreprise->isValid()) {
                $this->getDoctrine()->getManager()->persist($entreprise);
                $this->getDoctrine()->getManager()->flush(); 
        }
            
            return $this->render("/controller_prostage/formEntreprises.html.twig", ['vueFormulaireEntreprise'=>$formulaireEntreprise->createView(), 'action'=>"modifier"]);
            
        }
        
        
        
        
        
        /**
         * @Route("/formations", name="formations")
         */
        public function formations(){
            $repositeFormation = $this->getDoctrine()->getRepository(Formation::class);
            $formation= $repositeFormation->findAll();
            return $this->render("controller_prostage/formations.html.twig", ["listeFormation"=>$formation]); 
        }
        
        
        
        /**
         * @Route("/formations/{param}", name="stageParFormation")
        */   
        public function stageParFormation($param){
            $repositeStagListe = $this->getDoctrine()->getRepository(Stage::class); 
            $listeStage= $repositeStagListe->findByFormation($param);  
            return $this->render("controller_prostage/accueil.html.twig", ["listeStage"=>$listeStage]);
        }
        
        /**
         * @Route("/stages/{id}", name="stages")
         */
        public function stages($id, Stage $stage){




            return $this->render('controller_prostage/stages.html.twig', [
                'id' => $id, 'stage' => $stage
                ]);
                
                
            }

            
        /**
         * @Route("/creation_stage", name="ajouterStage")
         */
        public function ajouterStage(Request $request, ObjectManager $manager )
        {
            $stage = new Stage();
                
            $formulaireStage = $this->createForm(StageType::class, $stage);

            $formulaireStage->handleRequest($request) ;
                
            if ($formulaireStage->isSubmitted() && $formulaireStage->isValid()) {
                    $this->getDoctrine()->getManager()->persist($stage);
                    $this->getDoctrine()->getManager()->flush(); 
            }
                
            return $this->render("/controller_prostage/formStage.html.twig", ['vueFormulaireStage'=>$formulaireStage->createView()]);
                
        }

        
    }
    